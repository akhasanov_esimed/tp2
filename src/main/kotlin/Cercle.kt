import java.awt.geom.RoundRectangle2D

class Cercle(private val r:Double) : Figure(){

    override fun Perimetre(): Double {
        return 2 * Math.PI * r
    }

    override fun toString(): String {
        return "cercle"
    }
}