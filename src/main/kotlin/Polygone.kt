open class Polygone(val cotes:DoubleArray) : Figure() {
    override fun Perimetre(): Double {
        return cotes.sum()
    }
}