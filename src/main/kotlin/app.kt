

fun affichePerimetre(figure: Figure) {
    println("Périmètre de ${figure} : ${figure.Perimetre()}")
}

fun main(){
    val cercle = Cercle(2.0)
    val triangle = Triangle(3.0,2.0,1.0)
    affichePerimetre(cercle)
    affichePerimetre(triangle)
}