import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

class TestsTools {
    //val triangle = Triangle(0.0,1,5)
    @Test fun affichePerimetre(){
        val cercle = Cercle(2.0)
        val triangle = Triangle(3.0,2.0,1.0)
        Assertions.assertAll(
            Executable { Assertions.assertEquals(12.566370614359172, cercle.Perimetre()) },
            Executable { Assertions.assertEquals(6.0, triangle.Perimetre()) }
        )
    }
}